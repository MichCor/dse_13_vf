<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\User;

class UserController extends Controller
{
    public function pruebas (Request $request){
        return "Accion de pruebas de USER-CONTROLLER";
    }

    public function register (Request $request) {
//Recoger datos del usuario por post
        $json = $request ->input('json', null);
        $params = json_decode ($json); //objeto
        $params_array = json_decode ($json, true); //array

//Validar que no se envien datos vacios
        if (!empty($params_array) && !empty ($params_array)){


//Limpiar Datos
        $params_array = array_map('trim', $params_array);

//Validar datos
        $validate = \Validator::make ($params_array,[
            'name'      =>'required|alpha',
            'email'     =>'required|email|unique:users',
            'password'  =>'required'
        ]);

//valiacion incorrecta        
        if($validate->fails()){
            $data = array(
                'status'    => 'error',
                'code'      => '404',
                'mensaje'   => 'El usuario no se ha creado',
                'errors'    => $validate->errors()
            );
        }
        else {

//Validacion correcta
//Cifrar contraseña
           $pwd = password_hash($params->password, PASSWORD_BCRYPT, ['cost' =>4]);
//Crear usuario
            $user = New User();
            $user->name = $params_array['name'];
            $user->email = $params_array['email'];
            $user->password = $pwd;
            $user->save();

            $data = array(
                'status'    => 'success',
                'code'      => '200',
                'mensaje'   => 'El usuario se ha creado correctamente',
                'user'      => $user
            );
        }
    } else {
        $data = array(
            'status'    => 'error',
            'code'      => '404',
            'mensaje'   => 'Los datos enviados no son correctos'
        );
    }
        return response () ->json ($data,$data ['code']);
    }

    public function login (Request $request) {
        
        return "Accion de login usuarios";
    }
}
