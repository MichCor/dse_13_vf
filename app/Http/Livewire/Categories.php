<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Storage;
class Categories extends Component
{

    use WithFileUploads;
    use WithPagination;

    public $name, $search, $image , $selected_id,$pageTitle, $componentName;
    private $pagination=5;

    public function mount() {

        $this->pageTitle = 'Listado';
        $this->componentName = 'Categorias';
    }

    public function paginationView() {

        return 'vendor.livewire.bootstrap';
    }

    public function render()
    {
        if (strlen($this->search) > 0 )
        $data= category::where('name', 'like','%' . $this->search . '%')->paginate($this->pagination);
        else
        $data= category::orderBy('id','desc')->paginate($this->pagination);


        return view('livewire.category.categories', ['categories'=>$data])
        ->extends('layouts.theme.app')
        ->section('content');
    }


    public function Edit($id)
    {
        $record = Category::find($id, ['id','name','image']);
        $this->name = $record->name;
        $this->selected_id = $record->id;
        $this->image = null;

        $this->emit('show-modal','¡Hecho!');

    }

    public function Stored()

{
    $rules = [
        'name'=> 'required|unique:categories|min:3'
    ];

    $menssages = [

        'name.required'=> '¡Nombre es requerido!',
        'name.unique'=> '¡La categoria ya existe, debe ser unica!',
        'name.min'=> '¡El nombre debe ser minimo de 3 caracteres!'
    ];

    $this->validate($rules,$menssages);

    $category = category::create([

        'name' => $this->name,

    ]);

    $customFileName;
    if($this->image)
    {
        $customFileName = uniqid() . '_' . $this->image->extension();
        $this->image->storeAs('public/category',$customFileName);
        $category->image = $customFileName;
        $category->save();
    };

    $this->resetUI();
    $this->emit('category-added','¡Categoria Registrada!');
}



public function Update()
{
    $rules=[
        'name' => "required|min:3|unique:categories,name,{$this->selected_id}"
    ];

    $messages= [
        'name.required' => 'Nombre de categoria requerido',
        'name.min' => 'El nombre de la categoria debe tener al menos 3 caracteres',
        'name.unique' => 'El nombre de la categoria ya existe'
    ];

    $this->validate($rules, $messages);

    $category = category::find($this->selected_id);
    $category->update([
        'name' => $this->name
    ]);

    if($this->image){
        $customFileName = uniqid() . '_.' . $this->image->extension();
        $this->image->storeAs('public/category', $customFileName);
        $imageName = $category->image;

        $category->image = $customFileName;
        $category->save();

        if($imageName !=null){
            if(file_exists('storage/category' . $imageName)){
                unlink('public/category', $customFileName);
            }
        }

        


    }
    $this->resetUI();
    $this->emit('category-updated','Actualizado');
}



    public function resetUI()
    {
        $this-> name='';
        $this-> image= null;
        $this-> search='';
        $this-> selected_id=0;
    }

    protected $listeners = [
        'deleteRow' => 'Destroy'
    ];

    public function Destroy(category $category)
    {
        //$category=category::find($id);
        $imageName = $category->image;
        $category->delete();
        if($imageName !=null){
            unlink('storage/category/'. $imageName);
        }
        $this->resetUI();
        $this->emit('category-deleted' , 'Eliminada');
    }
}
