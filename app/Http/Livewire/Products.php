<?php

namespace App\Http\Livewire;
use Livewire\Component;
use App\Models\Category;
use App\Models\Product;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class Products extends Component
{
    use WithFileUploads;
    use WithPagination;

    public $name,$barcode,$price,$cost,$stock,$alerts,$categoryid,$search,$imagen,$selected_id,$pageTitle,
    $componentName;
    private $pagination =5;
    public function paginationView() {

        return 'vendor.livewire.bootstrap';
    }

    public function mount() {

        $this->pageTitle = 'Listado';
        $this->componentName = 'Productos';
        $this->categoryid = 'Elegir';
    }


    public function render()
    {
        if (strlen($this->search) > 0 )
        $products= Product::join ('categories as c , c.id, products.categoryid')
                    ->select ('products.*','c.name as category')
                    ->where('products.name','like', '%'. $this->search . '%' )
                    ->orWhere('products.barcode','like', '%'. $this->search . '%' )
                    ->orWhere('c.name','like', '%'. $this->search . '%' )
                    ->orderBy('products.name' ,'asc')
                    ->pagination($this->pagination);
                    
        else

        $products= Product::join ('categories as c , c.id, products.categoryid')
        ->select ('products.*','c.name as category')
        ->orderBy('products.name' ,'asc')
        ->pagination($this->pagination);

        return view('livewire.products'[
            'data' => products, 
            'category' => category::orderBy('name','asc')->get();   
        ]);
    }
}
