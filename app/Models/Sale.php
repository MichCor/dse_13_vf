<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;
    protected $table = 'sales';
//Relacion de uno a muchos
public function posts (){
    return $this->hasMany('App\Product');
}  

//relacion de uno a muchos, pero inversa (muchos a uno)    
public function user (){
    return $this->belongsTo('App\Models\User', 'user_id');
}

}
