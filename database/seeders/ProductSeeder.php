<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product:: create([
          'name'=> 'Norma cuadro chico',
          'cost'=> 30,
          'price'=> 40,
          'barcode'=>'00001a',
          'stock'=>1000,
          'alerts'=>20,
          'category_id'=>1,
          'image'=> 'nomra.png'
        ]);

        Product:: create([
          'name'=> 'big fino',
          'cost'=> 10,
          'price'=> 15,
          'barcode'=>'00002a',
          'stock'=>1000,
          'alerts'=>20,
          'category_id'=>2,
          'image'=> 'nomra.png'
        ]);

        Product:: create([
          'name'=> 'berol 2',
          'cost'=> 2,
          'price'=> 10,
          'barcode'=>'00003a',
          'stock'=>1000,
          'alerts'=>20,
          'category_id'=>3,
          'image'=> 'nomra.png'
        ]);

        Product:: create([
          'name'=> 'facties',
          'cost'=> 20,
          'price'=> 40,
          'barcode'=>'00004a',
          'stock'=>1000,
          'alerts'=>20,
          'category_id'=>4,
          'image'=> 'nomra.png'
        ]);

        //
    }
}
