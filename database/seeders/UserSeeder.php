<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User:: create([
        'name'=> 'test',
        'phone'=> 1234567890,
        'email'=> 'test@dse.com',
        'profile'=>'EMPLOYEE',
        'status'=>'ACTIVE',
        'password'=>bcrypt('Test.123'),
      ]);//
    }
}
