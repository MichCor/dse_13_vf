<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      Category::create([
        'name'=>'Libretas',
        'image'=>'https://dummyimage.com/200x150|/c4bec4/0011ff&text=DSE_13'
      ]);
      Category::create([
        'name'=>'Plumas',
        'image'=>'https://dummyimage.com/200x150|/c4bec4/0011ff&text=DSE_13'
      ]);
      Category::create([
        'name'=>'Lapices',
        'image'=>'https://dummyimage.com/200x150|/c4bec4/0011ff&text=DSE_13'
      ]);

      Category::create([
        'name'=>'Gomas',
        'image'=>'https://dummyimage.com/200x150|/c4bec4/0011ff&text=DSE_13'
      ]);
        //
    }
}
