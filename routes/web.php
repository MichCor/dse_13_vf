<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Categories;
use App\Http\Livewire\Categories;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('categories', Categories::class);
<<<<<<< HEAD
Route::get('products', Products::class);
=======

//Rutas de Prueba
Route:: get ('/test-orm', [App\Http\Controllers\PruebasController::class, 'testOrm']);
Route:: get ('/usuario/pruebas', [App\Http\Controllers\UserController::class, 'pruebas']);
Route:: get ('/category/pruebas', [App\Http\Controllers\CategoryController::class, 'pruebas']);
Route:: get ('/product/pruebas', [App\Http\Controllers\ProductController::class, 'pruebas']);
Route:: get ('/sale/pruebas', [App\Http\Controllers\SaleController::class, 'pruebas']);

//Rutas del controlador usuarios
Route::post ('/api/register', [App\Http\Controllers\UserController::class, 'register']);
Route::post ('/login', [App\Http\Controllers\UserController::class, 'login']);
>>>>>>> master
