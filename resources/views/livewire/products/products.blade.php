<div class="row sales layout-top-spacing">

<div class="col-sm-12">
<div class="widget widget-chart-one">
    <div class="widget-heading">
        <h4 class="card-title">
            <b>{{$componentName}} |  {{$pageTitle}}</b>
        </h4>
        <ul
            class="tabs tabs-pills">
            <li>
                <a href="javascrip:void(0)"class="tabmenu bg-dark" data-toggle="modal" data-target="#theModal">
                Agregar</a>
            </li>
        </ul>
    </div>
    @include('common.searchbox')

    <div class="widget-content">
        <div class="table-responsive">
            <table class="table table-bordered table-striped mt-1">
               <thead class="text-white" style=backgorund: #3B3F5C>
                <tr>
                    <th class="table-th text-white">DESCRIPCIÓN</th>
                    <th class="table-th text-white">CODIGO DE BARRAS</th>
                    <th class="table-th text-white">CATEGORIA</th>
                    <th class="table-th text-white">PRECIO</th>
                    <th class="table-th text-white">STOCK</th>
                    <th class="table-th text-white">INVENTARIO MIN</th>
                    <th class="table-th text-white">COSTO</th>
                    <th class="table-th text-white">IMAGEN</th>
                    <th class="table-th text-white">ACTION</th>
                </tr>
               </thead> 
               <tbody>
               @foreach{$data as $product}
                   <tr>
                          <td><h6>{{product->name}}</h6></td> 
                          <td><h6>{{product->barcode}}</h6></td> 
                          <td><h6>{{product->category}}</h6></td>  
                          <td><h6>{{product->price}}</h6></td>  
                          <td><h6>{{product->stock}}</h6></td>  
                          <td><h6>{{product->alerts}}</h6></td>  
                          <td><h6>{{product->cost}}</h6></td>  
                       <td class="text-center">
                          <span>
                             <img src="{{ asset('storage/products/'. $product->imagen)}}" alt="imagen de ejemplo" height="70" widht="80" class=rounded >
                             <a href="javascript:void(0)" 
                             wire:click.prevent="Edit({{$product->id}})"
                             class="btn btn-dark mtmobile" title="Editar">
                                 <i class="fas fa-edit"></i>
                             </a>
                             <a href="javascript:void(0)" 
                             onclick="confirm('{{$product->id}}' ,
                             '{{$category->products->count()}}')"
                             class="btn btn-dark" title="Delete">
                                 <i class="fas fa-trash">Eliminar</i>
                                 </a>
                          </span>
                       </td>
                   </tr>
                   @endforeach
               </tbody>
            </table>
            {{$data->links()}}
        </div>
    </div>
</div>
@include ('livewire.products.form')
</div>

<script>
    document.addeventlistener('DOMContentLoaded', function(){

          
        
        window.livewire.on('product-added', msg => {
            $('#theModal').modal('hide');
          
        });

        window.livewire.on('product-updated', msg => {
            $('#theModal').modal('hide');
          
        });
        window.livewire.on('product-deleted', msg => {
           //noty
        });

        window.livewire.on('modal-show', msg => {
            $('#theModal').modal('show');
          
        });
        window.livewire.on('modal-hide', msg => {
            $('#theModal').modal('hide');
          
        });
        window.livewire.on('hidden.bs.modal', msg => {
           $('.er').css('display','none')
          
        });






    });
</script>

</div>