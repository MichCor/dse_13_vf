@include('common.modalHead')
<div class="row">
<div class="col-sm-12 col-sm-8">
    <div class="form-group">
        <label>Nombre</label>
        <input type="text" wire:model.lazy="name" class="form-control" placeholder="Texto">
        @error(name) <span class="text-danger er">{{$message}}</span>@enderror
    </div>
</div>

<div class="col-sm-12 col-sm-4">
    <div class="form-group">
        <label>Codigo</label>
        <input type="text" wire:model.lazy="barcode" class="form-control" placeholder="00000">
        @error(barcode) <span class="text-danger er">{{$message}}</span>@enderror
    </div>
</div>

<div class="col-sm-12 col-sm-4">
    <div class="form-group">
        <label>Costo</label>
        <input type="text" data-type='currency' wire:model.lazy="cost" class="form-control" placeholder="0.0">
        @error(cost) <span class="text-danger er">{{$message}}</span>@enderror
    </div>
</div>
<div class="col-sm-12 col-sm-4">
    <div class="form-group">
        <label>Precio</label>
        <input type="text" data-type='currency' wire:model.lazy="price" class="form-control" placeholder="0.0">
        @error(price) <span class="text-danger er">{{$message}}</span>@enderror
    </div>
</div>
<div class="col-sm-12 col-sm-4">
    <div class="form-group">
        <label>Stock</label>
        <input type="number"  wire:model.lazy="stock" class="form-control" placeholder="0">
        @error(stock) <span class="text-danger er">{{$message}}</span>@enderror
    </div>
</div>
<div class="col-sm-12 col-sm-4">
    <div class="form-group">
        <label>Alertas</label>
        <input type="number"  wire:model.lazy="aletrs" class="form-control" placeholder="10">
        @error(alerts) <span class="text-danger er">{{$message}}</span>@enderror
    </div>
</div>
<div class="col-sm-12 col-sm-4">
    <div class="form-group">
        <label>Alertas</label>
        <select  class="form-control">
            <option value="Elegir" disabled>Elegir</option>
            @foreach($categories as $category)
            <option value="{{$category->id}}" disabled>{{category->name}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-sm-12 col-sm-4">
    <div class="form-control custom-file">
        <input type="file" class="custom-file-input form-control" wire-model="image" 
        accept="image/x-png, image/x-jpg,image/x-gif">
        <label class="custom-file-label">Imagen{{$image}}</label>
        @error('image') <span class="text-danger er">{{$message}}</span> @enderror
    </div>
</div>






</div>













@include(common.modalFooter)