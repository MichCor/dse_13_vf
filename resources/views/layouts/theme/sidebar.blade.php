<div class="sidebar-wrapper sidebar-theme">

          <nav id="compactSidebar">
              <ul class="menu-categories">

<li class="active">
  <a href="#" class="menu-toggle" date-actuve="true">
    <div class="base-menu">
      <div class="base-icons">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-activity"><polyline
        points="22 12 18 12 15 21 9 3 6 12 2 12"></polyline></svg>

</div>

<span>CATEGORIAS</span>
</div>
    <a/>
</li>

<li class="">
  <a href="#" class="menu-toggle" date-actuve="false">
    <div class="base-menu">
      <div class="base-icons">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-activity"><polyline
        points="22 12 18 12 15 21 9 3 6 12 2 12"></polyline></svg>

</div>

<span>PRODUCTOS</span>
</div>
    <a/>
</li>

<li class="active">
  <a href="#" class="menu-toggle" date-actuve="true">
    <div class="base-menu">
      <div class="base-icons">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
      stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
      class="feather feather-shopping-cart"><circle cx="9" cy="21" r="1"></circle><circle
      cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>

</div>

<span>VENTAS</span>
</div>
    <a/>
</li>

<li class="">
  <a href="#" class="menu-toggle" date-actuve="false">
    <div class="base-menu">
      <div class="base-icons">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
      stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users">
      <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle>
      <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>

</div>

<span>ROLES</span>
</div>
    <a/>
</li>

<li class="">
  <a href="#" class="menu-toggle" date-actuve="false">
    <div class="base-menu">
      <div class="base-icons">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
      stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-x">
      <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle>
      <line x1="18" y1="8" x2="23" y2="13"></line><line x1="23" y1="8" x2="18" y2="13"></line></svg>

</div>

<span>PERMISOS</span>
</div>
    <a/>
</li>

<li class="">
  <a href="#" class="menu-toggle" date-actuve="false">
    <div class="base-menu">
      <div class="base-icons">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
      stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-plus">
      <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle>
      <line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg>

</div>

<span>USUARIOS</span>
</div>
    <a/>
</li>

<li class="">
  <a href="#" class="menu-toggle" date-actuve="false">
    <div class="base-menu">
      <div class="base-icons">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
      stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-plus">
      <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle>
      <line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg>

</div>

<span>MONDEDAS</span>
</div>
    <a/>
</li>

<li class="">
  <a href="#" class="menu-toggle" date-actuve="false">
    <div class="base-menu">
      <div class="base-icons">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
      stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-plus">
      <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle>
      <line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg>

</div>

<span>ARQUEOS</span>
</div>
    <a/>
</li>

<li class="">
  <a href="#" class="menu-toggle" date-actuve="false">
    <div class="base-menu">
      <div class="base-icons">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
      stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-plus">
      <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle>
      <line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg>

</div>

<span>REPORTES</span>
</div>
    <a/>
</li>


</ul>
</nav>
</div>
