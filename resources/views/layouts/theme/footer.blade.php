<div class="footer-wrapper">
                <div class="footer-section f-section-1">
                    <p class="">DSE Systems Copyright © 2022 <a target="_blank" href="https://designreset.com"
                    >DesignReset</a>, All rights reserved.</p>
                </div>
                <div class="footer-section f-section-2">
                    <p class="">Coded with Destro software systems Version 1.21.12.1 </p>
                </div>
            </div>